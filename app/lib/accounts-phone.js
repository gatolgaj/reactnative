import Meteor, { call,Accounts } from 'react-native-meteor';
import SHA256 from 'sha256';
import _ from 'underscore'


// Attempt to log in with phone and password.
//
// @param selector {String|Object} One of the following:
//   - {phone: (phone)}
// @param password {String}
// @param callback {Function(error|undefined)}


/**
 * @summary Log the user in with a password.
 * @locus Client
 * @param {Object | String} user Either a string interpreted as a phone;
 *      or an object with a single key: `phone` or `id`.
 * @param {String} password The user's password.
 * @param {Function} [callback] Optional callback. Called with no arguments on success,
 *      or with a single `Error` argument on failure.
 */
Meteor.loginWithPhoneAndPassword = function (selector, password, callback) {
    if (typeof selector === 'string')
        selector = {phone: selector};

    call("login", {
        user: selector,
        password: Accounts._hashPassword(password)
    }, (err, result)=>{
      this._endLoggingIn();

      this._handleLoginCallback(err, result);

      typeof callback == 'function' && callback(err);
    })
};

Accounts._hashPassword = function (password) {
    return {
        digest   : SHA256(password),
        algorithm: "sha-256"
    };
};

// XXX COMPAT WITH 0.8.1.3
// The server requested an upgrade from the old SRP password format,
// so supply the needed SRP identity to login. Options:
//   - upgradeError: the error object that the server returned to tell
//     us to upgrade from SRP to bcrypt.
//   - userSelector: selector to retrieve the user object
//   - plaintextPassword: the password as a string
var srpUpgradePath = function (options, callback) {
    var details;
    try {
        details = EJSON.parse(options.upgradeError.details);
    } catch (e) {}
    if (!(details && details.format === 'srp')) {
        callback && callback(
            new Meteor.Error(400, "Password is old. Please reVerify phone again"));
    } else {
        Accounts.callLoginMethod({
            methodArguments: [
                {
                    user    : options.userSelector,
                    srp     : SHA256(details.identity + ":" + options.plaintextPassword),
                    password: Accounts._hashPassword(options.plaintextPassword)
                }
            ],
            userCallback   : callback
        });
    }
};

// Attempt to log in as a new user.

/**
 * @summary Create a new user with phone.
 * @locus Anywhere
 * @param {Object} options
 * @param {String} options.phone The user's full phone number.
 * @param {String} options.password, Optional -- (optional) The user's password. This is __not__ sent in plain text over the wire.
 * @param {Object} options.profile The user's profile, typically including the `name` field.
 * @param {Function} [callback] Client only, optional callback. Called with no arguments on success, or with a single `Error` argument on failure.
 */
Accounts.createUserWithPhone = function (options, callback) {
    options = _.clone(options); // we'll be modifying options

    // If no password was given create random one
    if (typeof options.password !== 'string' || !options.password) {
        options.password = Math.random().toString(36).slice(-8);
    }

    // Replace password with the hashed password.
    options.password = Accounts._hashPassword(options.password);

    Accounts.callLoginMethod({
        methodName     : 'createUserWithPhone',
        methodArguments: [options],
        userCallback   : callback
    });
};


// Sends an sms to a user with a code to verify his number.
//
// @param phone: (phone)
// @param callback (optional) {Function(error|undefined)}

/**
 * @summary Request a new verification code.
 * @locus Client
 * @param {String} phone -  The phone we send the verification code to.
 * @param {Function} [callback] Optional callback. Called with no arguments on success, or with a single `Error` argument on failure.
 */
Accounts.requestPhoneVerification = function (phone, callback) {
    if (!phone)
        throw new Error("Must pass phone");
    Accounts.connection.call("requestPhoneVerification", phone, callback);
};

// Verify phone number -
// Based on a code ( received by SMS ) originally created by
// Accounts.verifyPhone, optionally change password and then logs in the matching user.
//
// @param code {String}
// @param newPassword (optional) {String}
// @param callback (optional) {Function(error|undefined)}

/**
 * @summary Marks the user's phone as verified. Optional change passwords, Logs the user in afterwards..
 * @locus Client
 * @param {String} phone - The phone number we want to verify.
 * @param {String} code - The code retrieved in the SMS.
 * @param {String} newPassword, Optional, A new password for the user. This is __not__ sent in plain text over the wire.
 * @param {Function} [callback] Optional callback. Called with no arguments on success, or with a single `Error` argument on failure.
 */
Accounts.verifyPhone = function (phone, code, newPassword, callback) {
    check(code, String);
    check(phone, String);

    var hashedPassword;

    if (newPassword) {
        // If didn't gave newPassword and only callback was given
        if (typeof(newPassword) === 'function') {
            callback = newPassword;
        } else {
            check(newPassword, String);
            hashedPassword = Accounts._hashPassword(newPassword);
        }
    }
    Accounts.callLoginMethod({
        methodName     : 'verifyPhone',
        methodArguments: [phone, code, hashedPassword],
        userCallback   : callback});
};

/**
 * Returns whether the current user phone is verified
 * @returns {boolean} Whether the user phone is verified
 */
Accounts.isPhoneVerified = function () {
    var me = Meteor.user();
    return !!(me && me.phone && me.phone.verified);
};

Accounts.callLoginMethod = function (options) {
  var self = this;
     self._setLoggingIn = function(x) {
    if (this._loggingIn !== x) {
      this._loggingIn = x;
      this._loggingInDeps.changed();
    }
  }
console.log(self)
  options = _.extend({
    methodName: 'login',
    methodArguments: [{}],
    _suppressLoggingIn: false
  }, options);

  // Set defaults for callback arguments to no-op functions; make sure we
  // override falsey values too.
  _.each(['validateResult', 'userCallback'], function (f) {
    if (!options[f])
      options[f] = function () {};
  });

  // Prepare callbacks: user provided and onLogin/onLoginFailure hooks.
  var loginCallbacks = _.once(function (error) {
    if (!error) {
      self._onLoginHook.each(function (callback) {
        callback();
        return true;
      });
    } else {
      self._onLoginFailureHook.each(function (callback) {
        callback();
        return true;
      });
    }
    options.userCallback.apply(this, arguments);
  });

  var reconnected = false;

  // We want to set up onReconnect as soon as we get a result token back from
  // the server, without having to wait for subscriptions to rerun. This is
  // because if we disconnect and reconnect between getting the result and
  // getting the results of subscription rerun, we WILL NOT re-send this
  // method (because we never re-send methods whose results we've received)
  // but we WILL call loggedInAndDataReadyCallback at "reconnect quiesce"
  // time. This will lead to makeClientLoggedIn(result.id) even though we
  // haven't actually sent a login method!
  //
  // But by making sure that we send this "resume" login in that case (and
  // calling makeClientLoggedOut if it fails), we'll end up with an accurate
  // client-side userId. (It's important that livedata_connection guarantees
  // that the "reconnect quiesce"-time call to loggedInAndDataReadyCallback
  // will occur before the callback from the resume login call.)
  var onResultReceived = function (err, result) {
    if (err || !result || !result.token) {
      // Leave onReconnect alone if there was an error, so that if the user was
      // already logged in they will still get logged in on reconnect.
      // See issue #4970.
    } else {
      self.connection.onReconnect = function () {
        reconnected = true;
        // If our token was updated in storage, use the latest one.
        var storedToken = self._storedLoginToken();
        if (storedToken) {
          result = {
            token: storedToken,
            tokenExpires: self._storedLoginTokenExpires()
          };
        }
        if (! result.tokenExpires)
          result.tokenExpires = self._tokenExpiration(new Date());
        if (self._tokenExpiresSoon(result.tokenExpires)) {
          self.makeClientLoggedOut();
        } else {
          self.callLoginMethod({
            methodArguments: [{resume: result.token}],
            // Reconnect quiescence ensures that the user doesn't see an
            // intermediate state before the login method finishes. So we don't
            // need to show a logging-in animation.
            _suppressLoggingIn: true,
            userCallback: function (error) {
              var storedTokenNow = self._storedLoginToken();
              if (error) {
                // If we had a login error AND the current stored token is the
                // one that we tried to log in with, then declare ourselves
                // logged out. If there's a token in storage but it's not the
                // token that we tried to log in with, we don't know anything
                // about whether that token is valid or not, so do nothing. The
                // periodic localStorage poll will decide if we are logged in or
                // out with this token, if it hasn't already. Of course, even
                // with this check, another tab could insert a new valid token
                // immediately before we clear localStorage here, which would
                // lead to both tabs being logged out, but by checking the token
                // in storage right now we hope to make that unlikely to happen.
                //
                // If there is no token in storage right now, we don't have to
                // do anything; whatever code removed the token from storage was
                // responsible for calling `makeClientLoggedOut()`, or the
                // periodic localStorage poll will call `makeClientLoggedOut`
                // eventually if another tab wiped the token from storage.
                if (storedTokenNow && storedTokenNow === result.token) {
                  self.makeClientLoggedOut();
                }
              }
              // Possibly a weird callback to call, but better than nothing if
              // there is a reconnect between "login result received" and "data
              // ready".
              loginCallbacks(error);
            }});
        }
      };
    }
  };
// This callback is called once the local cache of the current-user
  // subscription (and all subscriptions, in fact) are guaranteed to be up to
  // date.
  var loggedInAndDataReadyCallback = function (error, result) {
    // If the login method returns its result but the connection is lost
    // before the data is in the local cache, it'll set an onReconnect (see
    // above). The onReconnect will try to log in using the token, and *it*
    // will call userCallback via its own version of this
    // loggedInAndDataReadyCallback. So we don't have to do anything here.
    if (reconnected)
      return;

    // Note that we need to call this even if _suppressLoggingIn is true,
    // because it could be matching a _setLoggingIn(true) from a
    // half-completed pre-reconnect login method.
    self._setLoggingIn(false);
    if (error || !result) {
      error = error || new Error(
        "No result from call to " + options.methodName);
      loginCallbacks(error);
      return;
    }
    try {
      options.validateResult(result);
    } catch (e) {
      loginCallbacks(e);
      return;
    }

    // Make the client logged in. (The user data should already be loaded!)
    self.makeClientLoggedIn(result.id, result.token, result.tokenExpires);
    loginCallbacks();
  };
  //if (!options._suppressLoggingIn)
  //  self._setLoggingIn(true);
  self.connection.apply(
    options.methodName,
    options.methodArguments,
    {wait: true, onResultReceived: onResultReceived},
    loggedInAndDataReadyCallback);


};
export { Meteor };
export {Accounts};
