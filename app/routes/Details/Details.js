import React, { PropTypes } from 'react';
import { View, ScrollView } from 'react-native';
import Loading from '../../components/Loading';
import styles from './styles';
import { Image } from 'react-native';
import { Button,Separator,Container, Content, List, ListItem, Text,Left,Thumbnail,Body,CardItem,Card,Item,Grid,Row } from 'native-base';

const _renderList = ({products}) => {
  
}
const Details = ({ detailsReady, details }) => {
  if (!detailsReady) {
    return <Loading />;
  }

  return (
            <Container>
              <Content>
                {details.map((detail) => (
                  <Grid>
                    <Row>
                     <Separator bordered>
                        <Text>{detail.categoryName} ({detail.products.length})</Text>
                    </Separator>
                    </Row>
                    <Row>
                <Content horizontal >
                      {detail.products.map((product) => (
                      <Card> 
                        
                          <CardItem cardBody >
                                  <Image style={{ resizeMode: 'cover',width: 100, height: 200, flex: 1 }} source={{uri:"https://test.ytal.live/"+product.images[0]}} />
                          </CardItem>
                          <CardItem>
                              <Body>
                                  <Text>{product.name}</Text>
                                  <Text note>{product.description}</Text>
                                  <Button small primary>
                        <Text>Add To Basket</Text>
                    </Button>
                              </Body>
                          </CardItem>
                        
                      </Card> 
                      )) }
       
                </Content>
                </Row>
                </Grid>
                 ))}
                 </Content>
            </Container>

 
  );
};

Details.propTypes = {
  detailsReady: PropTypes.bool,
  details: PropTypes.array,
};

Details.defaultProps = {
  details: [],
};

export default Details;
